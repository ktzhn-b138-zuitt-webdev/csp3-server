const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Create product (ADMIN)
router.post("/create", auth.verify, (req, res) => {
			
			const data = auth.decode(req.headers.authorization)

			productController.addProduct(data, req.body).then(resultFromController => res.send(resultFromController));

		});

// ALL products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController));
})

// All active products
router.get("/", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));
})

// Specific product
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));

})

// Updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	productController.updateProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})

// Archiving a product
router.put("/:productId/archive", auth.verify, (req, res) => {
	const user = auth.decode(req.headers.authorization)
	console.log(req.params.productId);
	productController.archiveProduct(user, req.params, req.body).then(resultFromController => res.send(resultFromController));
})


module.exports = router;
