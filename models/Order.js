const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount : {
		type: Number,
		required: [true, "Required"]
	},
	purchasedOn : {
		type: Date,
		default: new Date()
	},
	orderUserId : {
		type: String
	},
	orderProducts: [{ 
		productId: {type: String},
		name: {type: String}, 
		numberOf: {type: Number},
		price: {type: Number}
	}]
	
});

module.exports = mongoose.model("Order", orderSchema);

