const Product = require("../models/Product");

// Create new product
module.exports.addProduct = async (user, reqBody) => {
	if(user.isAdmin){
		let newProduct = new Product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false;
			}
			else{
				console.log(`New product created.`)
				return true;
			}
		})
	}
	else{
		console.log(`You are not authorized to create a product.`)
		return false;
	}
}

// All products, including archived products
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}

// Retrieve all ACTIVE products
module.exports.getActiveProducts = () => {
	return Product.find({isActive : true}).then(result => {
		return result;
	})
}

// Get specific product 
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}


// Update a product
module.exports.updateProduct = async (user, reqParams, reqBody) => {
	if(user.isAdmin){
		let updatedProduct = {
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		}

		return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				console.log(`Product successfully updated.`)
				return true;
			}
		})
	}else{
		return (`You are not authorized to update products.`);
	}
}

// Archive a product
module.exports.archiveProduct = async (user, reqParams, reqBody) => {

	if(user.isAdmin){

		let archiveProduct = {
			isActive : reqBody.isActive
		}

		return Product.findByIdAndUpdate(reqParams.productId, archiveProduct).then((product, error) => {
			if(error){
				return false;
			}
			else{
				console.log(`Product successfully archived.`)
				return true;
			}
		})
	}
	else{
		return (`You are not authorized to archive products.`);
		return false;
	}
}